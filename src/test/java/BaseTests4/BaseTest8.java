package BaseTests4;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.concurrent.TimeUnit;

import static junit.framework.Assert.assertEquals;

/**
 * Created with IntelliJ IDEA.
 * User: georgiy
 * Date: 19.04.18
 * Time: 12:35
 * To change this template use File | Settings | File Templates.
 */
public class BaseTest8 {
    WebDriver driver = initChromeDriver();
    @Test
    public void firstTest(){
        driver.get("http://test1.cmstest.t.ukrtech.info/backend" );
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        WebElement field = driver.findElement(By.id("LoginForm_email"));
        field.sendKeys("admin");
        WebElement password = driver.findElement(By.id("LoginForm_password"));
        password.sendKeys("12345678");
        WebElement button = driver.findElement(By.id("yw0"));
        button.click();
        WebElement kategorii = driver.findElement(By.linkText("Категории"));
        kategorii.click();
        WebElement dobavit = driver.findElement(By.linkText("Добавить"));
        dobavit.click();
        WebElement name = driver.findElement(By.id("Category_name")) ;
        name.sendKeys("CatCat");
        WebElement slug = driver.findElement(By.id("Category_slug"));
        slug.sendKeys("catcat");
        WebElement picture = driver.findElement(By.id("Category_image"));
        picture.click();

        WebElement button1 = driver.findElement(By.id("yw0"));
        button1.click();
        WebElement main = driver.findElement
                (By.cssSelector("#overall-wrap > nav > div > div.navbar-header > a > img"));
        main.click();
        WebElement kategorii1 = driver.findElement(By.linkText("Категории"));
        kategorii1.click();
        WebElement newKategore = driver.findElement(By.xpath("//*[@id=\"category-grid\"]/table/tbody/tr[4]/td[3]/a"));
        String newKategoreText = newKategore.getText();
        assertEquals("CatCat",newKategoreText);
        driver.quit();

    }
    public static WebDriver initChromeDriver() {
        System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir") + "/drivers/chromedriver.exe");
        return new ChromeDriver();
    }
}
